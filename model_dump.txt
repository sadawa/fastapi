En général, lorsqu'on parle de sérialisation, on fait référence à la conversion d'objets Python en une représentation JSON (ou d'autres formats de données). Désérialisation, quant à elle, est le processus inverse, où des données JSON sont converties en objets Python.

Pydantic, par défaut, offre des méthodes pour la sérialisation et la désérialisation. Celles-ci sont dict() et json().

dict() convertit un objet Pydantic en un dictionnaire Python.
json() convertit un objet Pydantic en une chaîne JSON.
Voici un exemple pour illustrer leur utilisation :

python

from pydantic import BaseModel

class Item(BaseModel):
    name: str
    description: str = None
    price: float
    tax: float = None

item_data = {"name": "Product", "price": 10.99}

# Créer un objet Pydantic à partir d'un dictionnaire
item_object = Item(**item_data)

# Sérialiser l'objet Pydantic en un dictionnaire
item_dict = item_object.dict()
print(item_dict)

# Sérialiser l'objet Pydantic en JSON
item_json = item_object.json()
print(item_json)

# Désérialiser un dictionnaire en objet Pydantic
new_item_object = Item(**item_dict)
print(new_item_object)

# Désérialiser une chaîne JSON en objet Pydantic
new_item_object_from_json = Item.parse_raw(item_json)
print(new_item_object_from_json)
Dans cet exemple :

Nous définissons un modèle Pydantic Item avec certains champs.
Nous créons un objet Pydantic item_object à partir d'un dictionnaire item_data.
Nous utilisons les méthodes dict() et json() pour sérialiser item_object en un dictionnaire et une chaîne JSON respectivement.
Nous utilisons **item_dict pour désérialiser le dictionnaire en un nouvel objet Pydantic new_item_object.
Nous utilisons Item.parse_raw() pour désérialiser la chaîne JSON en un nouvel objet Pydantic new_item_object_from_json.
Cela illustre comment vous pouvez utiliser la sérialisation et la désérialisation avec Pydantic pour travailler avec des données dans vos applications FastAPI.