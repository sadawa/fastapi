"""add column content in table posts

Revision ID: 5496352d5f19
Revises: d8fd1d7779fc
Create Date: 2024-07-03 16:23:23.547550

"""

from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = "5496352d5f19"
down_revision: Union[str, None] = "d8fd1d7779fc"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column("posts", sa.Column("content", sa.String(), nullable=False))
    pass


def downgrade() -> None:
    op.drop_column("posts", "content")
    pass
