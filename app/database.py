from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import psycopg2
from psycopg2.extras import RealDictCursor
import time
from .config import settings


SQLALCHEMY_DATABASE_URL = f"postgresql://{settings.database_username}:{settings.database_password}@{settings.database_hostname}:{settings.database_port}/{settings.database_name}"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
)


SessionLocal = sessionmaker(autoflush=False, autocommit=False, bind=engine)

Base = declarative_base()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


# Create connection in database postgres Sql
# while True:
#     try:
#         conn = psycopg2.connect(
#             host="localhost",
#             database="fastApi",
#             user="postgres",
#             password="shiro93270",
#             cursor_factory=RealDictCursor,
#         )
#         cursor = conn.cursor()
#         print("Database is connected in db ")
#         break
#     except Exception as e:
#         print(e)
#         print("Database is not connected in db ")
#         time.sleep(5)
